/* 
util.promisify方法
*/
// 引入util模块
const util = require('util')
// 引入fs模块
const fs = require('fs')
// 返回一个新函数
let mineReadFile = util.promisify(fs.readFile)

let path = require('path');

mineReadFile(path.resolve(__dirname, './data/content.txt')).then(res=>{
  console.log(res.toString())
},error=>{
  console.log(error)
})