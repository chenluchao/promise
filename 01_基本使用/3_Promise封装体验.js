/* 
 * 封装一个mineReadFile 读取文件内容
 * 参数：path 文件路径
 * 返回：Promise 对象
*/
var fs = require('fs');
let path = require('path');

function mineReadFile(filePath){
  filePath = path.resolve(__dirname, filePath);
  return new Promise((resolve,reject)=>{
    fs.readFile(filePath,(err,data)=>{
      if(err) reject(err);
      resolve(data)
    })
  })
}
mineReadFile('./data/content.txt').then(res=>{
  console.log(res.toString())
},err=>{
  console.log('读取失败',err)
})