function Promise(excutor) {
  // 添加属性
  this.PromiseState = 'pending'
  this.PromiseResault = null
  // 声明一个对象
  this.callbacks = []
  // 保存实例对象的this值
  const _this = this
  // resolve函数
  function resolve(data) {
    // 判断状态-promise状态只允许修改一次
    if (_this.PromiseState !== 'pending') return
    // 1、修改对象状态（promiseState）
    _this.PromiseState = 'fulfilled'
    // 2、设置对象结果值（promiseResault）
    _this.PromiseResault = data
    // 调用成功的回调函数
    _this.callbacks.forEach((item) => {
      item.onResolved(data)
    })
  }
  // reject函数
  function reject(data) {
    // 判断状态-promise状态只允许修改一次
    if (_this.PromiseState !== 'pending') return
    // 1、修改对象状态（promiseState）
    _this.PromiseState = 'rejected'
    // 2、设置对象结果值（promiseResault）
    _this.PromiseResault = data
    _this.callbacks.forEach((item) => {
      item.onRejected(data)
    })
  }
  // 使用try-catch实现throw逻辑
  try {
    // 不同调用执行器函数
    excutor(resolve, reject)
  } catch (error) {
    reject(error)
  }
}

// 添加then方法
Promise.prototype.then = function (onResolved, onRejected) {
  const _this = this
  // 判断回调函数参数
  if (typeof onRejected !== 'function') {
    onRejected = (reason) => {
      throw reason
    }
  }
  if (typeof onResolved !== 'function') {
    onResolved = (value) => value
  }
  return new Promise((resolve, reject) => {
    // 封装回调函数
    function callback(type) {
      try {
        // 获取回调函数执行结果
        let result = type(_this.PromiseResault)
        // 判断
        if (result instanceof Promise) {
          // 如果是Promise类型对象
          result.then(
            (v) => {
              resolve(v)
            },
            (r) => {
              reject(r)
            }
          )
        } else {
          // 结果的对象状态为成功
          resolve(result)
        }
      } catch (error) {
        reject(error)
      }
    }
    // 调用回调函数
    if (this.PromiseState === 'fulfilled') {
      callback(onResolved)
    }
    if (this.PromiseState === 'rejected') {
      callback(onRejected)
    }
    // 判断pending状态
    if (this.PromiseState === 'pending') {
      // 保存回调函数
      this.callbacks.push({
        onResolved: function () {
          callback(onResolved)
        },
        onRejected: function () {
          callback(onRejected)
        },
      })
    }
  })
}

// 添加catch方法
Promise.prototype.catch = function (onRejected) {
  return this.then(undefined, onRejected)
}
