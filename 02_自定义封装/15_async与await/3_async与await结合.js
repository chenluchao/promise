const path = require('path')
const fs = require('fs')
const util = require('util')
let mineReadFile = util.promisify(fs.readFile)
// 使用回调函数实现
// fs.readFile(path.resolve(__dirname, './data/1.txt'), (err, data1) => {
//   if (err) throw err
//   fs.readFile(path.resolve(__dirname, './data/2.txt'), (err, data2) => {
//     if (err) throw err
//     fs.readFile(path.resolve(__dirname, './data/3.txt'), (err, data3) => {
//       if (err) throw err
//       console.log(data1 + data2 + data3)
//     })
//   })
// })
async function main() {
  // 读取第一个文件内容
  try {
    let data1 = await mineReadFile(path.resolve(__dirname, './data/1.txt'))
    let data2 = await mineReadFile(path.resolve(__dirname, './data/2.txt'))
    let data3 = await mineReadFile(path.resolve(__dirname, './data/3.txt'))
    console.log('async-await', data1 + data2 + data3)
  } catch (error) {
    console.warn(error)
  }
}
main()
