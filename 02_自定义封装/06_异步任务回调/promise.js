function Promise(excutor) {
  // 添加属性
  this.PromiseState = 'pending'
  this.PromiseResault = null
  // 声明一个对象
  this.callback = {}
  // 保存实例对象的this值
  const _this = this
  // resolve函数
  function resolve(data) {
    // 判断状态-promise状态只允许修改一次
    if (_this.PromiseState !== 'pending') return
    // 1、修改对象状态（promiseState）
    _this.PromiseState = 'fulfilled'
    // 2、设置对象结果值（promiseResault）
    _this.PromiseResault = data
    // 调用成功的回调函数
    if(_this.callback.onResolved){
      _this.callback.onResolved(data)
    }
  }
  // reject函数
  function reject(data) {
    // 判断状态-promise状态只允许修改一次
    if (_this.PromiseState !== 'pending') return
    // 1、修改对象状态（promiseState）
    _this.PromiseState = 'rejected'
    // 2、设置对象结果值（promiseResault）
    _this.PromiseResault = data
    if(_this.callback.onRejected){
      _this.callback.onRejected(data)
    }
  }
  // 使用try-catch实现throw逻辑
  try {
    // 不同调用执行器函数
    excutor(resolve, reject)
  } catch (error) {
    reject(error)
  }
}
Promise.prototype.then = function (onResolved, onRejected) {
  // 调用回调函数
  if (this.PromiseState === 'fulfilled') {
    onResolved(this.PromiseResault)
  }
  if (this.PromiseState === 'rejected') {
    onRejected(this.PromiseResault)
  }
  // 判断pending状态
  if (this.PromiseState === 'pending') {
    // 保存回调函数
    this.callback = {
      onResolved,
      onRejected,
    }
  }
}
