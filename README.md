# 学习记录引导
## Promise理解与使用
* Promise是ES6异步编程的新技术方案
* Promise支持链式调用，解决回调地狱问题
## Ajax请求
```javascript
const xhr = new XMLHttpRequest()
xhr.open('GET','url')
xhr.send()
xhr.onreadystatechange = function(){
  if(xhr.readyState === 4){
    if(xhr.status >= 200 && xhr.status < 300){
      // 控制台输出响应体
      console.log(xhr.response)
    }else{
      // 控制台输出响应状态码
      console.log(xhr.status)
    }
  }
}
```
## Promise状态
* 实例对象中的一个属性 [PromiseStatel]
1. 状态
* pedding 未决定的
* resolved / fullfilled 成功
* rejected 失败
2. 状态改变
* pedding变为resolved
* pedding变为rejected
* 说明：只有这两种，且一个promise对象只能改变一次，无论变为成功还是失败，都会有一个结果数据。
## Promise对象的值
* 实例对象中的一个属性 [PromiseResult]——保存着对象[成功/失败]的结果
* resolve
* reject
## Promise的基本流程
```javascript
                             ---成功了执行resolve()->promise对象->回调onResolved()--
                          ---                       resolved状态     then()        -
new Promise--->执行异步操作                                                        返回新的promise对象
                          ---                                                     -
                             ---失败了执行reject()->promise对象->回调onReject() --
                                                   rejected状态    then()/catch()
```
* 备注：
new新的promise对象，进行异步操作，如果操作成功会执行`resolve()`并且把状态修改为`resolved`，然后会执行then里面的第一个回调（成功回调）；如果操作失败会执行`reject()`并且把状态修改为`rejected`，然后会执行then里面的第二个回调（失败回调）；最终会返回新的promise对象。
## Promise-Api
### Promise构造函数:promise(excutor){}
1. executor函数：执行器(resolve,reject)=>{}
2. resolve函数:内部定义成功时我们调用的函数value=>{}
3. reject函数:内部定义失败时我们调用的函数reason=>{}
说明：executor会在Promise内部立即同步执行，异步操作在执行器中执行
### Promise.prototype.then方法：(onResolved,onRejected)=>{}
1. onResolved()函数：成功的回调函数(value)=>{}
2. onRejected()函数：失败的回调函数(reason)=>{}
说明：指定用于得到成功value的成功回调和用于得到失败reason的失败回调返回一个新的promise对象

### Promise.prototype.catch方法：(onRejected)=>{}
1. onRejected函数：失败的回调函数(reason)=>{}
### Promise.resolve()函数
```javascript
let p = Promise.resolve(521)
// 如果闯入的参数为 非Promise类型的对象，则返回的结果为成功promise对象
// 如果闯入的参数为 Promise类型的对象，则参数的结果决定了resolve的结果
// 无论传如什么返回的Promise对象状态都是失败的
```
### Promise.reject()函数
```javascript
let p = Promise.reject(521)
// 如果闯入的参数为 非Promise类型的对象，则返回的结果为失败promise对象
// 如果闯入的参数为 Promise类型的对象，返回的结果依旧为失败promise对象
```
### Promise.all()函数
* Promise包含n个Promise的数组
* 返回一个新的Promise，只有所有的Promise都成功才成功且成功的返回的Promise结果值是所有的Promise的值组成的数组，只要有一个失败就失败且返回的Promise结果值就是失败的哪个Promise返回的值。
### Promise.race()函数
* Promise:包含n个Promise数组
* 返回一个新的Promise，第一个完成的Promise的结果状态就是最终的结果状态
## Promise几个关键问题
### Promise改变状态与指定回调的顺序问题
* 都有可能，正常情况下是先指定回调再改变状态，单页可以先该状态在指定回调
* 如何先改变状态再回调？
  1. 在执行器中直接调用resolve()/reject()
  2. 延迟更长时间才调用then()
* 当`executor函数：执行器(resolve,reject)=>{}`中是异步时会先指定回调再修改状态
* 什么时候得到数据
  1. 如果先指定的回调，那当状态发生改变时，回调函数就会调用，得到数据
  2. 如果先改变状态，那当指定回调时，回调函数就会调用，得到数据
### then方法返回结果有什么决定
1. 简单表达：有then()指定的回调函数执行的结果决定
2. 详细表达：
  * 如果抛出异常，新promise变为rejected,reason为抛出的异常
  * 如果但会的是非promise的任意值，新promise编为resolved,value为返回的值
  * 如果返回的是另一个新promise，此promise的结果就会成为新promise的结果
### Promise串联任务
* promise的then()返回一个新的promise，可以开发成then()的链式调用
* 通过then的链式调用串联多个同步/异步任务
### Promise异常穿透
* 中断promise的唯一方法：返回一个pedding状态的promise

```javascript
let p = = new Promise((resolve, reject) => {
  resolve('OK');
})
p.then(()=>{
  console.log('111')
  return new Promise(()=>{})//返回一个新的pedding状态的Promise中断后续执行
}).then(()=>{
  console.log('222')
}).then(()=>{
  console.log('333')
}).catch(error=>{
  console.log(error)
})
```
